console.log("Start");

const API = "http://localhost:3000";

let dataCollection = [];

async function getData() {
  try {
    const response = await fetch(`${API}/data`);
    console.log("response: ", response);
    const data = await response.json();
    return data;
  } catch {
    throw new Error("Fetch data fail");
  }
}

async function postData(dataToPost) {
  try {
    const response = await fetch(`${API}/data`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(dataToPost),
    });
    const data = await response.json();
    return data;
  } catch {
    throw new Error("Post data fail");
  }
}

getData()
  .then((res) => {
    dataCollection = res;
    displayDataList(dataCollection);
    console.log("fetch dataCollection: ", dataCollection);
  })
  .catch((err) => {
    console.error("New error", err);
  });

//--

const btnSend = document.getElementById("btnsend");

btnSend.addEventListener("click", (evt) => {
  console.log("click evt: ", evt);
  const formToPost = getFormData();
  console.log("formToPost: ", formToPost);
  postData(formToPost)
    .then(() => {
      return getData();
    })
    .then((res) => {
      dataCollection = res;
      displayDataList(dataCollection);
      console.log("fetch dataCollection 2: ", dataCollection);
    })
    .catch((err) => {
      console.error("New error", err);
    });
});

function getFormData() {
  const name = document.getElementById("name").value;
  const score = document.getElementById("score").value;
  const id = crypto.randomUUID();
  return { name, score, id };
}

function displayDataList(dataList) {
  const list = document.getElementById("datalist");
  list.innerHTML = "";
  const liCollection = dataList.map((elm) => {
    const li = document.createElement("li");
    li.innerHTML = `${elm.name} - ${elm.score}`;
    return li;
  });
  console.log("liCollection: ", liCollection);
  liCollection.forEach((li) => {
    list.appendChild(li);
  });
}
