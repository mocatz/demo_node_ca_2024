import fs from "node:fs";
import cors from "cors";
import express from "express";
import { cities } from "./cities.js";
import { netflix } from "./netflix.js";

const app = express();
app.use(express.json());
app.use(cors());
const PORT = process.env.PORT || 3000;

const myData = cities;
const netflixFilm = netflix;
const QUIZZ_DATA_PATH =
  "/Users/jonathancourat/Documents/BAG-ERA/Formation_vuejs_campus_2023/api_demo2/quizz.json";

function getQuizzData() {
  fs.readFile(QUIZZ_DATA_PATH, "utf8", (err, data) => {
    if (err) {
      console.error("error on get quizz file: ", err);
      quizz = [];
    }
    quizz = JSON.parse(data);
  });
}

let quizz = [];
getQuizzData();

app.get("/data", (request, response) => {
  console.log("get datas");
  response.send(myData);
});

app.get("/netflix", (request, response) => {
  console.log("get netflix data");
  response.send(netflixFilm);
});

app.post("/data", (request, response) => {
  if (!request.body.id) {
    response.status(400);
    response.send("No id on the item");
  } else {
    myData.push(request.body);
    response.status(200);
    response.send(request.body);
  }
});

// -- QUIZZ API ----------------------------------------------------------------
app.get("/quizz", (request, response) => {
  console.log("Get quizz: ", quizz);
  response.send(quizz);
});

app.post("/quizz", (request, response) => {
  quizz.push(request.body);
  const content = JSON.stringify(quizz);
  fs.writeFile(QUIZZ_DATA_PATH, content, (err) => {
    if (err) {
      console.error(err);
    } else {
      console.log("QUIZZ FILE SAVED");
      response.status(200);
      response.send(request.body);
    }
  });
});

app.delete("/quizz/:id", (request, response) => {
  const quizzIdToDelete = request.params.id;
  quizz = quizz.filter((elm) => elm.id !== quizzIdToDelete);
  const content = JSON.stringify(quizz);
  fs.writeFile(QUIZZ_DATA_PATH, content, (err) => {
    if (err) {
      console.error(err);
    } else {
      console.log("QUIZZ FILE SAVED");
      response.status(200);
      response.send(request.body);
    }
  });
  response.status(200);
  response.send({ id: quizzIdToDelete });
});

app.listen(PORT, () => {
  console.log("Server Listening on PORT:", PORT, "http://localhost:3000/");
  console.log("GET data", "http://localhost:3000/data");
});
